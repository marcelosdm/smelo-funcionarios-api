package com.luizalabs.funcionarios.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.luizalabs.funcionarios.business.FuncionarioBusiness;
import com.luizalabs.funcionarios.repository.FuncionarioRepository;

@Configuration
public class FakeConfiguration {
	
	private FuncionarioRepository funcionarioRepository;
	
	@Bean
	public FuncionarioBusiness funcionarioBusiness() {
		return new FuncionarioBusiness(this.funcionarioRepository, null);
		
	}

}
