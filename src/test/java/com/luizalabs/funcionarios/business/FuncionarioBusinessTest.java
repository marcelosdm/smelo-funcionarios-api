package com.luizalabs.funcionarios.business;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.luizalabs.funcionarios.config.FakeConfiguration;
import com.luizalabs.funcionarios.entity.Funcionario;
import com.luizalabs.funcionarios.entity.Status;
import com.luizalabs.funcionarios.error.ErrorBusiness;
import com.luizalabs.funcionarios.error.NotFoundError;
import com.luizalabs.funcionarios.repository.FuncionarioRepository;
import com.luizalabs.funcionarios.util.DateUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = FakeConfiguration.class)
public class FuncionarioBusinessTest {

	@Mock
	private FuncionarioRepository funcionarioRepository;
	
	@Autowired
	private DateUtil dateUtil;

	@Test(expected = NotFoundError.class)
	public void funcionarioNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		List<Funcionario> funcionarios = new ArrayList<>();
		Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);

		funcionarioBusiness.getFuncionarios();
	}

	@Test
	public void getFuncionariosTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		List<Funcionario> funcionarios = new ArrayList<>();
		Funcionario funcionario = new Funcionario();
		funcionario.setNome("TESTE");
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionarios().getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionarioPorNomeNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Mockito.when(funcionarioRepository.findByNomeStartingWith(Mockito.anyString())).thenReturn(null);

		funcionarioBusiness.getFuncionarioPorNome(null);
	}

	@Test
	public void getFuncionarioPorNomeTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		List<Funcionario> funcionarios = new ArrayList<>();
		Funcionario funcionario = new Funcionario();
		funcionario.setNome("TESTE");
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findByNomeStartingWith("TESTE")).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionarioPorNome("TESTE").getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionarioPorCpfNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Mockito.when(funcionarioRepository.findByCpf(Mockito.anyString())).thenReturn(null);

		funcionarioBusiness.getFuncionarioPorCpf("123");
	}

	@Test
	public void getFuncionarioPorCpfTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		String cpf = Mockito.anyString();

		Funcionario funcionario = new Funcionario();
		funcionario.setCpf(cpf);

		Mockito.when(funcionarioRepository.findByCpf(cpf)).thenReturn(funcionario);

		assertEquals(200, funcionarioBusiness.getFuncionarioPorCpf(cpf).getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionarioPorCargoNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Mockito.when(funcionarioRepository.findByCargoStartingWith(Mockito.anyString())).thenReturn(null);
		
		funcionarioBusiness.getFuncionariosPorCargo(null);

	}

	@Test
	public void getFuncionariosPorCargoTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		String cargo = Mockito.anyString();
		
		List<Funcionario> funcionarios = new ArrayList<>();

		Funcionario funcionario = new Funcionario();
		funcionario.setCargo(cargo);
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findByCargoStartingWith(cargo)).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionariosPorCargo(cargo).getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionariosPorStatusNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Mockito.when(funcionarioRepository.findByStatus(Status.ATIVO)).thenReturn(null);

		funcionarioBusiness.getFuncionariosPorStatus(null);
	}

	@Test
	public void getFuncionariosPorStatusTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Status status = Status.ATIVO;
		
		List<Funcionario> funcionarios = new ArrayList<>();

		Funcionario funcionario = new Funcionario();
		funcionario.setStatus(status);
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findByStatus(status)).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionariosPorStatus(status).getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionariosPorFaixaSalarialNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Double faixaInicial = Mockito.anyDouble();
		Double faixaFinal = Mockito.anyDouble();

		Mockito.when(funcionarioRepository.findBySalarioBetween(faixaInicial, faixaFinal)).thenReturn(null);

		funcionarioBusiness.getFuncionariosPorFaixaSalarial(faixaInicial, faixaFinal);
	}
	
	@Test(expected = ErrorBusiness.class)
	public void funcionariosPorFaixaSalarialBadRequest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);
		
		Double faixaInicial = 2000.00;
		Double faixaFinal = 1000.00;
		
		Mockito.when(funcionarioRepository.findBySalarioBetween(faixaInicial, faixaFinal)).thenReturn(null);
		
		funcionarioBusiness.getFuncionariosPorFaixaSalarial(faixaInicial, faixaFinal);
	}

	@Test
	public void getFuncionariosPorFaixaSalarialTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Double faixaInicial = 0.00;
		Double faixaFinal = 2000.00;
		
		List<Funcionario> funcionarios = new ArrayList<>();
		Funcionario funcionario = new Funcionario();
		funcionario.setSalario(1000);
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findBySalarioBetween(faixaInicial, faixaFinal)).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionariosPorFaixaSalarial(faixaInicial, faixaFinal).getStatusCodeValue());
	}

	@Test(expected = NotFoundError.class)
	public void funcionariosPorDataCadastroNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		String dataCad = "02/02/2020";
		
		LocalDate dataParam = dateUtil.converteLocalDate(dataCad);
		
		Mockito.when(funcionarioRepository.findByDataCad(dataParam)).thenReturn(null);

		funcionarioBusiness.getFuncionariosPorDataCadastro(dataCad);
	}

	@Test
	public void getFuncionariosPorDataCadastroTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		String dataCad = "02/02/2020";
		
		LocalDate dataParam = dateUtil.converteLocalDate(dataCad);
		
		List<Funcionario> funcionarios = new ArrayList<>();
		Funcionario funcionario = new Funcionario();
		funcionario.setDataCad(dataParam);
		funcionarios.add(funcionario);

		Mockito.when(funcionarioRepository.findByDataCad(dataParam)).thenReturn(funcionarios);

		assertEquals(200, funcionarioBusiness.getFuncionariosPorDataCadastro(dataCad).getStatusCodeValue());

	}

	@Test
	public void cadastraFuncionarioTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Funcionario funcionario = new Funcionario();
		funcionario.setCpf(Mockito.anyString());

		Mockito.when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);

		assertEquals(201, funcionarioBusiness.cadastraFuncionario(funcionario).getStatusCodeValue());

	}

	@Test(expected = NotFoundError.class)
	public void removeFuncionarioNotFoundTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		funcionarioBusiness.removeFuncionario(Mockito.anyString());
	}

	@Test
	public void removeFuncionarioTest() {
		FuncionarioBusiness funcionarioBusiness = new FuncionarioBusiness(funcionarioRepository, null);

		Mockito.doNothing().when(funcionarioRepository).delete(Mockito.any());

		Mockito.when(funcionarioRepository.findByCpf(Mockito.anyString())).thenReturn(new Funcionario());

		assertEquals(204, funcionarioBusiness.removeFuncionario(Mockito.anyString()).getStatusCodeValue());
	}

}
