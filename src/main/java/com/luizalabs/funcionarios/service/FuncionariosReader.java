package com.luizalabs.funcionarios.service;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.luizalabs.funcionarios.business.FuncionarioBusiness;
import com.luizalabs.funcionarios.entity.Funcionario;
import com.luizalabs.funcionarios.entity.Status;
import com.luizalabs.funcionarios.repository.FuncionarioDTO;
import com.luizalabs.funcionarios.util.DateUtil;

@Service
public class FuncionariosReader {

	private static final Logger logger = LoggerFactory.getLogger(FuncionariosReader.class);

	@Autowired
	private FuncionarioDTO funcionarioDTO;

	@Autowired
	private DateUtil dateUtil;

	String file = "src/main/resources/funcionarios.txt";
	BufferedReader br = null;
	String line = "";
	String splitBy = ";";

	public void saveFuncionariosData() {

		if (funcionarioDTO.count() == 0) {

			try {
				br = new BufferedReader(new FileReader(file));
				br.readLine();
//			String line1 = null;
				while ((line = br.readLine()) != null) {

					String[] dados = line.split(splitBy);
					Funcionario f = new Funcionario();

					LocalDate dataParam = dateUtil.converteLocalDate(dados[0]);
					f.setDataCad(dataParam);
					f.setCargo(dados[1]);
					f.setCpf(dados[2]);
					f.setNome(dados[3]);
					f.setUf(dados[4]);
					f.setSalario(Double.parseDouble(dados[5]));
					f.setStatus(Status.valueOf(dados[6]));

					funcionarioDTO.save(f);

					logger.info("Importação de Funcionários realizada com sucesso!");

				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			logger.info("Dados já importados!");
		}
	}
}
