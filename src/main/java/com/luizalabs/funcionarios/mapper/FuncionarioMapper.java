package com.luizalabs.funcionarios.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.luizalabs.funcionarios.entity.FuncionarioUF;

public class FuncionarioMapper implements RowMapper<FuncionarioUF> {

	@Override
	public FuncionarioUF mapRow(ResultSet rs, int rowNum) throws SQLException {
		
		FuncionarioUF funcionario = new FuncionarioUF();
		
		funcionario.setFuncionarios(rs.getLong("funcionarios"));
		funcionario.setUf(rs.getString("uf_nasc"));
		
		return funcionario;
	}
}
