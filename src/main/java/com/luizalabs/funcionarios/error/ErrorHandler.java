package com.luizalabs.funcionarios.error;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {
	
	@ExceptionHandler(value = {NotFoundError.class})
	public ResponseEntity<?> notFoundError(NotFoundError e) {
		
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setDeveloperMessage(e.getMessage());
		errorMessage.setUserMessage(e.getLocalizedMessage());
		errorMessage.setUrl("https://www.spring-desafio.com/errors");
		
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorMessage);
	}
	
	@ExceptionHandler(value = {ErrorBusiness.class})
	public ResponseEntity<?> errorBusiness(ErrorBusiness e){
		
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setDeveloperMessage(e.getMessage());
		errorMessage.setUserMessage(e.getLocalizedMessage());
		errorMessage.setUrl("https://www.spring-desafio.com/errors");
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
	}
	
	@ExceptionHandler(value = {MethodArgumentNotValidException.class})
	public ResponseEntity<?> notFoundError(MethodArgumentNotValidException e) {
		
		List<FieldError> errors = e.getBindingResult().getFieldErrors();
		List<String> errorMsg = new ArrayList<>();
		for(FieldError err: errors) {
			errorMsg.add(String.format("[%s] - %s", err.getField(), err.getDefaultMessage()));
		}
		
		ErrorMessage errorMessage = new ErrorMessage();
		errorMessage.setDeveloperMessage(String.join(",", errorMsg));
		errorMessage.setUserMessage(String.join(",", errorMsg));
		errorMessage.setUrl("https://www.spring-desafio.com/errors");
		
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorMessage);
	}

}
