package com.luizalabs.funcionarios.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.luizalabs.funcionarios.business.FuncionarioBusiness;
import com.luizalabs.funcionarios.entity.Funcionario;
import com.luizalabs.funcionarios.entity.FuncionarioUF;
import com.luizalabs.funcionarios.entity.Status;

import io.swagger.annotations.ApiOperation;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping(path = "v1")
public class FuncionarioController {

	private FuncionarioBusiness funcionarioBusiness;

	@Autowired
	public FuncionarioController(FuncionarioBusiness funcionarioBusiness) {
		super();
		this.funcionarioBusiness = funcionarioBusiness;
	}

	@GetMapping(path = "funcionarios")
	@ApiOperation(value = "Retorna/busca todos os funcionários cadastrados", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionarios() {
		return funcionarioBusiness.getFuncionarios();
	}

	
	@GetMapping(path = "funcionarios/nome")
	@ApiOperation(value = "Retorna/busca funcionários por nome", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionarioPorNome(@RequestParam("nome") String nome) {
		return funcionarioBusiness.getFuncionarioPorNome(nome);
	}

	@GetMapping(path = "funcionarios/cpf")
	@ApiOperation(value = "Retorna/busca funcionário por CPF", response = Funcionario[].class)
	public ResponseEntity<Funcionario> getFuncionarioPorCpf(@RequestParam("cpf") String cpf) {
		return funcionarioBusiness.getFuncionarioPorCpf(cpf);
	}

	@GetMapping(path = "funcionarios/cargo")
	@ApiOperation(value = "Retorna/busca funcionários por cargo", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionariosPorCargo(@RequestParam("cargo") String cargo) {
		return funcionarioBusiness.getFuncionariosPorCargo(cargo);
	}

	@GetMapping(path = "funcionarios/dataCad")
	@ApiOperation(value = "Retorna/busca funcionários por data de cadastro", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionariosPorDataCad(@RequestParam("dataCad") String dataCad) {
		
		return funcionarioBusiness.getFuncionariosPorDataCadastro(dataCad);
	}

	@GetMapping(path = "funcionarios/uf")
	@ApiOperation(value = "Retorna/busca funcionários por UF de nascimento, de forma quantitativa", response = Funcionario[].class)
	public ResponseEntity<List<FuncionarioUF>> getCountFuncionariosPorUfNasc() {
		return funcionarioBusiness.getCountFuncionariosPorUfNasc();
	}
	
	@GetMapping(path = "funcionarios/status")
	@ApiOperation(value = "Retorna/busca funcionários por status", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionarioPorStatus(@RequestParam("status")Status status) {
		return funcionarioBusiness.getFuncionariosPorStatus(status);
	}
	
	@GetMapping(path = "funcionarios/salario")
	@ApiOperation(value = "Retorna/busca funcionários faixa salarial", response = Funcionario[].class)
	public ResponseEntity<List<Funcionario>> getFuncionarioPorFaixaSalarial(@RequestParam double faixaInicial, @RequestParam double faixaFinal){
		return funcionarioBusiness.getFuncionariosPorFaixaSalarial(faixaInicial, faixaFinal);
	}

	@PostMapping(path = "funcionarios")
	@ApiOperation(value = "Inclui novo funcionário ou atualiza caso já exista", response = Funcionario[].class)
	public ResponseEntity<Funcionario> cadastraFuncionario(@RequestBody Funcionario funcionario) {
		return funcionarioBusiness.cadastraFuncionario(funcionario);
	}
	
	@DeleteMapping(path="funcionarios/cpf")
	@ApiOperation(value = "Remove um funcionário pelo número do CPF", response = Funcionario[].class)
	public ResponseEntity<?> removeFuncionario(@RequestParam("cpf")String cpf) {
		return funcionarioBusiness.removeFuncionario(cpf);
	}
}
