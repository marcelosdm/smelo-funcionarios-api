package com.luizalabs.funcionarios.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@Entity()
@Table(name = "funcionario", uniqueConstraints = { @UniqueConstraint(columnNames = { "cpf" }) })

@JsonInclude(Include.NON_NULL)
public class Funcionario {

	@Id
	@Column(name = "cpf", length = 11)
	@Size(min = 11, max = 11)
	@NotNull
	private String cpf;

	@Column(name = "dataCad", updatable = false)
	private LocalDate dataCad;

	@Column(name = "cargo")
	private String cargo;

	@Column(name = "nome")
	private String nome;

	@Column(name = "ufNasc", length = 2)
	@Size(min = 2, max = 2)
	private String uf;

	@Column(name = "salario")
	private double salario;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	private Status status = Status.ATIVO;

	public LocalDate getDataCad() {
		return dataCad;
	}

	public void setDataCad(LocalDate dataCad) {
		this.dataCad = dataCad;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

}
