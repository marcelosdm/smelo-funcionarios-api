package com.luizalabs.funcionarios.entity;

public class FuncionarioUF {
	private String uf;
	private Long funcionarios;

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Long getFuncionarios() {
		return funcionarios;
	}

	public void setFuncionarios(Long funcionarios) {
		this.funcionarios = funcionarios;
	}

}
