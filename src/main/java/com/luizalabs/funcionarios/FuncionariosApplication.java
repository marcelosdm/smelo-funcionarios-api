package com.luizalabs.funcionarios;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.luizalabs.funcionarios.service.FuncionariosReader;

@SpringBootApplication
public class FuncionariosApplication {
	

	public static void main(String[] args) {
		SpringApplication.run(FuncionariosApplication.class, args);
	}
	
	@Autowired
	private FuncionariosReader funcionariosReader;
	
	@PostConstruct
	public void init() {
		funcionariosReader.saveFuncionariosData();
	}
	

}
