package com.luizalabs.funcionarios.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.stereotype.Component;

@Component
public class DateUtil {
	
	/**
	 * Método responsável por converter datas no formato String para o formato LocalDate
	 * @param data
	 * @return 
	 */
	public LocalDate converteLocalDate(String data) {
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		LocalDate dateTime = LocalDate.parse(data, formatter);
		
		return dateTime;
	}

}
