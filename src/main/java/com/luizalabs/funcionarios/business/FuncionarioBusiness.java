package com.luizalabs.funcionarios.business;

import java.time.LocalDate;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.luizalabs.funcionarios.entity.Funcionario;
import com.luizalabs.funcionarios.entity.FuncionarioUF;
import com.luizalabs.funcionarios.entity.Status;
import com.luizalabs.funcionarios.error.ErrorBusiness;
import com.luizalabs.funcionarios.error.NotFoundError;
import com.luizalabs.funcionarios.repository.FuncionarioDAO;
import com.luizalabs.funcionarios.repository.FuncionarioRepository;
import com.luizalabs.funcionarios.util.DateUtil;

@Component
public class FuncionarioBusiness {

	private static final Logger logger = LoggerFactory.getLogger(FuncionarioBusiness.class);

	private FuncionarioRepository funcionarioRepository;
	private FuncionarioDAO funcionarioDAO;
	private DateUtil dateUtil;

	@Autowired
	public FuncionarioBusiness(FuncionarioRepository funcionarioRepository, FuncionarioDAO funcionarioDAO) {
		super();
		this.funcionarioRepository = funcionarioRepository;
		this.funcionarioDAO = funcionarioDAO;
		this.dateUtil = dateUtil;
	}

	/**
	 * Método para retornar todos os funcionarios cadastrados
	 * 
	 * @return
	 */
	public ResponseEntity<List<Funcionario>> getFuncionarios() {
		List<Funcionario> funcionarios = funcionarioRepository.findAll();

		if (funcionarios.isEmpty()) {
			throw new NotFoundError("Ainda não existem funcionários cadastrados!");
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para retornar funcionários por nome
	 * 
	 * @param nome
	 * @return
	 */
	public ResponseEntity<List<Funcionario>> getFuncionarioPorNome(String nome) {

		List<Funcionario> funcionarios = funcionarioRepository.findByNomeStartingWith(nome);

		if (funcionarios.isEmpty()) {
			throw new NotFoundError("Não foi encontrado nenhum funcionário com o nome " + nome);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para retornar um funcionário por CPF
	 * 
	 * @param cpf
	 * @return
	 */
	public ResponseEntity<Funcionario> getFuncionarioPorCpf(String cpf) {
		Funcionario funcionario = funcionarioRepository.findByCpf(cpf);

		if (funcionario == null) {
			throw new NotFoundError("Não foi encontrado funcionário com o CPF " + cpf);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionario);
		}
	}

	/**
	 * Método para retornar funcionários por cargo
	 * 
	 * @param cargo
	 * @return
	 */
	public ResponseEntity<List<Funcionario>> getFuncionariosPorCargo(String cargo) {
		List<Funcionario> funcionarios = funcionarioRepository.findByCargoStartingWith(cargo);

		if (funcionarios.isEmpty()) {
			throw new NotFoundError("Não foi encontrado nenhum funcionário com o cargo " + cargo);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para retornar funcionarios por data de cadastro
	 * 
	 * @param dataCad
	 * @return
	 */
	public ResponseEntity<List<Funcionario>> getFuncionariosPorDataCadastro(String dataCad) {

		LocalDate dataParam = dateUtil.converteLocalDate(dataCad);
		List<Funcionario> funcionarios = funcionarioRepository.findByDataCad(dataParam);

		if (funcionarios == null || funcionarios.isEmpty()) {
			throw new NotFoundError("Não foi encontrado nenhum funcionário cadastrado na data " + dataCad);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para buscar funcionários agrupados por UF de Nascimento, de forma
	 * quantitativa
	 * 
	 * @return
	 */
	public ResponseEntity<List<FuncionarioUF>> getCountFuncionariosPorUfNasc() {
		List<FuncionarioUF> funcionarios = funcionarioDAO.countFuncionarioByUf();

		if (funcionarios.isEmpty()) {
			throw new NotFoundError("Não há funcionários com UF Nascimento cadastrada.");
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para buscar funcionarios por Status
	 * 
	 * @param status
	 * @return
	 */

	public ResponseEntity<List<Funcionario>> getFuncionariosPorStatus(Status status) {
		List<Funcionario> funcionarios = funcionarioRepository.findByStatus(status);

		if (funcionarios == null || funcionarios.isEmpty()) {
			throw new NotFoundError("Não foi encontrado nenhum funcionário com o status " + status);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para buscar funcionários por Faixa Salarial
	 * 
	 * @param faixaInicial
	 * @param faixaFinal
	 * @return
	 */
	public ResponseEntity<List<Funcionario>> getFuncionariosPorFaixaSalarial(double faixaInicial, double faixaFinal) {
		List<Funcionario> funcionarios = funcionarioRepository.findBySalarioBetween(faixaInicial, faixaFinal);

		if (faixaInicial > faixaFinal) {
			throw new ErrorBusiness("A faixa inicial deve ser menor que a faixa final");
		} else if (funcionarios == null || funcionarios.isEmpty()) {
			throw new NotFoundError("Não foi encontrado nenhum funcionario na faixa salarial entre " + faixaInicial
					+ " e " + faixaFinal);
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(funcionarios);
		}
	}

	/**
	 * Método para cadastrar um novo funcionario ou atualizar caso já exista
	 * 
	 * @param funcionario
	 * @return
	 */
	public ResponseEntity<Funcionario> cadastraFuncionario(Funcionario funcionario) {
		try {
			funcionario = funcionarioRepository.save(funcionario);
		} catch (Exception e) {
			logger.error("Falha ao salvar funcionario: " + e.getMessage());
		}

		return ResponseEntity.status(HttpStatus.CREATED).body(funcionario);
	}

	/**
	 * Método para remover um funcionário pelo número do CPF
	 * 
	 * @param cpf
	 * @return
	 */
	public ResponseEntity<?> removeFuncionario(String cpf) {

		Funcionario funcionario = funcionarioRepository.findByCpf(cpf);
		if (funcionario != null) {
			funcionarioRepository.delete(funcionario);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} else {
			throw new NotFoundError("Não foi encontrado funcionário com o CPF " + cpf);
		}

	}

}
