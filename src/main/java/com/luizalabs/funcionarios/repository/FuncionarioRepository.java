package com.luizalabs.funcionarios.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.luizalabs.funcionarios.entity.Funcionario;
import com.luizalabs.funcionarios.entity.Status;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {

	public List<Funcionario> findByNomeStartingWith(String nome);

	public Funcionario findByCpf(String cpf);

	public List<Funcionario> findByCargoStartingWith(String cargo);

	public List<Funcionario> findByDataCad(LocalDate dataCad);

	public List<Funcionario> findByStatus(Status status);

	public List<Funcionario> findBySalarioBetween(double faixaInicial, double faixaFinal);

}
