package com.luizalabs.funcionarios.repository;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.luizalabs.funcionarios.entity.FuncionarioUF;
import com.luizalabs.funcionarios.mapper.FuncionarioMapper;

@Repository
public class FuncionarioDAO {

	private JdbcTemplate jdbcTemplate;

	public FuncionarioDAO(JdbcTemplate jdcJdbcTemplate) {
		super();
		this.jdbcTemplate = jdcJdbcTemplate;
	}
	
	public List<FuncionarioUF> countFuncionarioByUf() {
		String sql = "select count(cpf) as funcionarios, uf_nasc from funcionario group by uf_nasc";
		
		List<FuncionarioUF> funcionarios = jdbcTemplate.query(sql, new FuncionarioMapper());
		
		return funcionarios;
	}
	
}


