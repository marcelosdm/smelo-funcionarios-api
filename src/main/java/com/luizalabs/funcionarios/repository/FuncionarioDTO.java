package com.luizalabs.funcionarios.repository;

import org.springframework.data.repository.CrudRepository;

import com.luizalabs.funcionarios.entity.Funcionario;

public interface FuncionarioDTO extends CrudRepository<Funcionario, String> {

}
